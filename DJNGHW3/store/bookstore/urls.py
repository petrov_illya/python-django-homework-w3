from django.urls import path
from .views import *

urlpatterns = [
    path('', book_list, name='book_list'),
    path('book/<int:book_id>', book_details, name='book_details'),
    path('author/<int:author_id>', author_details, name='author_details'),
    path('author/<int:author_id>/books', author_books, name='author_books')
]

