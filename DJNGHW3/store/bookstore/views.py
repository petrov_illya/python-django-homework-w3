from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from .booksore_db import *


def book_list(request):
    return render(request, 'book_list.html', context={'books': books})


def book_details(request, book_id):
    book_info = books[book_id - 1]
    a_id = book_info['author_id']
    author_info = authors[a_id - 1]
    return render(request, 'book.html', context={'book_info': book_info,
                                                 'author_info': author_info})


def author_details(request, author_id):
    author_info = authors[author_id - 1]
    return render(request, 'author.html', context=author_info)


def author_books(request, author_id):
    a_books = []
    author_info = authors[author_id - 1]
    for i in books:
        if i['author_id'] == author_id:
            a_books.append({'id': i['id'], 'title': i['title']})
    return render(request, 'author_books.html', context={'author_books': a_books,
                                                         'author_info': author_info})
