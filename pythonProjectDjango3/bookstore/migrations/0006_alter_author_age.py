# Generated by Django 4.0.6 on 2022-07-17 13:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookstore', '0005_alter_author_age'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='age',
            field=models.CharField(max_length=100),
        ),
    ]
