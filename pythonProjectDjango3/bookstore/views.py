from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.http import HttpRequest, HttpResponse
from .models import Book, Author
from .forms import AddBookForm


def book_list(request):
    books = Book.objects.all().order_by('-id')
    if request.method == 'GET' and 'search_book_input' in request.GET:
        books = Book.objects.filter(title__icontains=request.GET['search_book_input'])
    context = {'books': books}
    return render(request, 'book_list.html', context=context)


def book_details(request, book_id):
    book_info = get_object_or_404(Book, id=book_id)
    author_id = book_info.author_id
    author_info = get_object_or_404(Author, id=author_id)
    context = {'book_info': book_info, 'author_info': author_info}
    return render(request, 'book.html', context=context)


def author_details(request, author_id):
    author_info = get_object_or_404(Author, id=author_id)
    context = {'author_info': author_info}
    return render(request, 'author.html', context=context)


def author_books(request, author_id):
    a_books = get_list_or_404(Book, author_id=author_id)
    author_info = get_object_or_404(Author, id=author_id)
    context = {'author_books': a_books, 'author_info': author_info}
    return render(request, 'author_books.html', context=context)


def add_book(request):
    if request.method == 'POST':
        form = AddBookForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('book_list')
    context = {'add_book_form': AddBookForm}
    return render(request, 'add_book.html', context=context)
